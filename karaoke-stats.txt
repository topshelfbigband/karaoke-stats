
=HOW MANY SINGERS SANG EACH CHART AT TSBB KARAOKE?=

Top Shelf Big Band does periodic "Big Band Karaoke" shows:

    https://www.topshelfbigband.com/karaoke/

Below are notes on how many singers sang each of our songs at each
of the shows.  This helps us prioritize what to rehearse for future
karaoke shows.

==============================================================================

('n' == not offered)
                                    2019-11-10  2020-03-08  2022-11-19  2023-04-22  2023-09-16  2023-11-18
  41  Fly Me to the Moon            3           1           1           2           1           0
  42  Walkin My Baby Back Home      0           0           0           0           n           n
 300  Cheek to Cheek                1           2           2           1           0           1
 312  Fever                         2           2           2           0           1           0
 367  More                          n           n           n           0           1           0
 371  Summer Wind                   1           0           1           0           1           1
 373  The Best is Yet to Come       0           1           1           1           1           0
 432  You Make Me Feel So Young     1           0           1           1           0           1
 434  T’aint What You Do            0           1           n           n           n           n
 436  Too Darn Hot                  0           1           2           0           4           0
 438  Witchcraft                    2           0           1           0           0           2
 494  It’s all Right With Me        3           0           1           1           0           0
 500  A Tisket, A Tasket            1           0           1           0           0           0
 529  It Happened in Monterey       1           0           0           0           0           0
 530  I Believe in You              0           0           n           1           0           0
 532  I’ve Got You Under My Skin    2           0           1           1           1           1
 543  Bei Mir Bist du Schoen        1           n           n           n           n           n
 545  New York, New York            1           0           0           1           2           1
 546  Jump, Jive, and Wail          1           0           n           n           n           n
 547  A Lot of Living to Do         1           n           n           n           n           n
 548  Paper Moon                    1           0           1           2           0           0
 549  My Way                        1           1           1           1           2           3
 550  Strangers in the Night        1           0           1           0           0           0
 551  Beyond the Sea                2           1           0           1           1           0
 553  Big Spender                   1           0           n           1           1           1
 555  Just A Gigolo                 0           0           n           n           n           n
 556  Sway                          1           0           1           2           0           1
 558  Day In, Day Out               1           0           n           n           n           n
 559  Come Fly With Me              2           1           1           1           0           1
 560  Time After Time               0           0           0           1           0           1
 561  Blue Skies                    1           1           1           1           1           1
 562  Why Don’t You Do Right?       2           1           n           n           n           n
 563  Ain’t Misbehavin              0           0           0           0           n           n
 564  Dead Man’s Party              1           n           n           n           n           n
 578  L-O-V-E                       n           1           2           1           2           1
 579  Moondance                     n           1           1           0           2           0
 580  Night and Day                 n           0           n           n           n           n
 582  Mack the Knife                n           1           2           2           1           1
 583  The Tender Trap               n           0           n           n           n           n
 596  Can’t Take My Eyes Off of You n           n           3           1           2           2
 644  The Way You Look Tonight      n           n           n           n           1           1
 645  Luck Be a Lady                n           n           n           n           1           2
 653  Feeling Good                  n           n           n           n           n           2
 655  The Lady is a Tramp           n           n           n           n           1           0
 663  Sentimental Journey           n           n           n           1           0           0
 691  You Belong To me              n           n           n           1           0           0
 693  Orange Colored Sky            n           n           n           1           2           2
 694  It’s Not Unusual              n           n           n           0           1           0
 697  Straighten Up and Fly Right   n           n           n           1           0           0
 712  Almost Like Being in Love     n           n           n           n           0           0
 713  I Only Have Eyes For You      n           n           n           n           0           1
 724  The Tender Trap               n           n           n           n           n           1
 725  Fascinating Rhythm            n           n           n           n           n           1
 732  Puttin on the Ritz            n           n           n           n           n           1

NOTE: I don't appear to have records from the 2020-03-08 show.
